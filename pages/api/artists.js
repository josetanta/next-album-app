let artists = [
  { name: 'Taylor Swift' },
  { name: 'Bon Jovi' },
  { name: 'Sofia' },
  { name: 'The Strokes' },
];

export default function handler(_req, res) {
  res.status(200).json(artists);
}
