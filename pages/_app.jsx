import { TrackProvider } from 'src/contexts';
import 'styles/globals.css';
import 'boxicons/css/boxicons.css';

function AlbumApp({ Component, pageProps }) {
  return (
    <TrackProvider key='track-provider'>
      <Component {...pageProps} />
    </TrackProvider>
  );
}

export default AlbumApp;
