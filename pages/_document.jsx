import { Head, Html, Main, NextScript } from 'next/document';

function AlbumDocument() {
  return (
    <Html lang='es'>
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}

export default AlbumDocument;
