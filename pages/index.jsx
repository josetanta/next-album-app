import React from 'react';
import NextImage from 'next/image';
import NextLink from 'next/link';

import Footer from 'src/components/Footer';
import Layout from 'src/components/Layout';
import AnimateWrapper from 'src/components/animate/AnimateWrapper';

function HomePage() {
  return (
    <React.Fragment>
      <Layout className='relative'>
        <AnimateWrapper timing='ease-out' typeAnimate='hidden-b'>
          <h1 className='mt-6 text-center text-base font-bold tracking-wider text-slate-700 dark:text-slate-300 sm:text-3xl md:text-5xl'>
            The PlayList
          </h1>
          <div className='my-4 h-1 w-full rounded-md bg-amber-700' />
          <div className='grid grid-cols-1'>
            <div className='group banner'>
              <AnimateWrapper timing='ease-out' typeAnimate='hidden-l'>
                <div className='banner__content order-2 md:order-none md:text-right'>
                  <NextLink scroll href='/artists'>
                    <a className='banner__content__link md:group-hover:text-indigo-400'>
                      Artists
                    </a>
                  </NextLink>
                  <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Iure ipsam quaerat dolore quod iste, id impedit odio
                    voluptatum a velit aperiam mollitia architecto eum
                    consequatur. Ratione corrupti, debitis omnis delectus labore
                    velit ipsa sint excepturi nostrum cum aliquam laboriosam
                    deleniti voluptas? Molestiae sequi sint, animi doloribus
                    nesciunt iusto eligendi voluptatibus.
                  </p>
                </div>
              </AnimateWrapper>

              <div className='banner__image order-1 md:order-none md:rotate-6'>
                <AnimateWrapper timing='ease-out' typeAnimate='hidden-r'>
                  <NextImage
                    className='banner__image--scale'
                    src='https://images.unsplash.com/photo-1575285113814-f770cb8c796e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80'
                    width={440}
                    height={500}
                    placeholder='empty'
                    loading='lazy'
                    alt='Concert Live - Music'
                  />
                </AnimateWrapper>
              </div>
            </div>
            <div className='group banner'>
              <div className='banner__image order-1 md:order-none md:-rotate-3'>
                <AnimateWrapper timing='ease-out' typeAnimate='hidden-l'>
                  <NextImage
                    className='banner__image--scale'
                    src='https://images.unsplash.com/photo-1459749411175-04bf5292ceea?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80'
                    width={470}
                    height={250}
                    loading='lazy'
                    placeholder='empty'
                    alt='Concert Live - Music'
                  />
                </AnimateWrapper>
              </div>

              <AnimateWrapper timing='ease-out' typeAnimate='hidden-r'>
                <div className='banner__content order-2 md:text-left'>
                  <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Iure ipsam quaerat dolore quod iste, id impedit odio
                    voluptatum a velit aperiam mollitia architecto eum
                    consequatur. Ratione corrupti, debitis omnis delectus labore
                    velit ipsa sint excepturi nostrum cum aliquam laboriosam
                    deleniti voluptas? Molestiae sequi sint, animi doloribus
                    nesciunt iusto eligendi voluptatibus.
                  </p>
                </div>
              </AnimateWrapper>
            </div>
          </div>

          <div className='my-4 h-1 w-full rounded-md bg-emerald-700' />
          <div className='grid grid-cols-1'>
            <div className='group banner'>
              <div className='banner__content order-2 md:order-none md:text-right'>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iure
                  ipsam quaerat dolore quod iste, id impedit odio voluptatum a
                  velit aperiam mollitia architecto eum consequatur. Ratione
                  corrupti, debitis omnis delectus labore velit ipsa sint
                  excepturi nostrum cum aliquam laboriosam deleniti voluptas?
                  Molestiae sequi sint, animi doloribus nesciunt iusto eligendi
                  voluptatibus.
                </p>
              </div>
              <div className='banner__image order-1 md:order-1 md:rotate-6'>
                <NextImage
                  className='banner__image--scale'
                  src='https://images.unsplash.com/photo-1499415479124-43c32433a620?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1332&q=80'
                  width={470}
                  height={250}
                  placeholder='empty'
                  loading='lazy'
                  alt='Concert Live - Music'
                />
              </div>
            </div>
            <div className='group banner'>
              <div className='banner__image order-3 md:order-none md:-rotate-3'>
                <NextImage
                  className='banner__image--scale'
                  src='https://images.unsplash.com/photo-1461360228754-6e81c478b882?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1174&q=80'
                  width={470}
                  height={250}
                  placeholder='empty'
                  loading='lazy'
                  alt='Albums Collection - Music'
                />
              </div>
              <div className='banner__content order-4 md:order-none md:text-left'>
                <NextLink scroll href='/albums'>
                  <a className='banner__content__link group-hover:text-amber-700'>
                    Albums
                  </a>
                </NextLink>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iure
                  ipsam quaerat dolore quod iste, id impedit odio voluptatum a
                  velit aperiam mollitia architecto eum consequatur. Ratione
                  corrupti, debitis omnis delectus labore velit ipsa sint
                  excepturi nostrum cum aliquam laboriosam deleniti voluptas?
                  Molestiae sequi sint, animi doloribus nesciunt iusto eligendi
                  voluptatibus.
                </p>
              </div>
            </div>
          </div>
        </AnimateWrapper>
      </Layout>
      <Footer />
    </React.Fragment>
  );
}

export default HomePage;
