import Layout from 'src/components/Layout';

function ArtistSinglePage(props) {
  return (
    <Layout title={`Artist | ${props.data.name.firstname}`}>
      <h1 className='text-5xl font-medium tracking-wider'>ArtistSingle</h1>
      <span>
        {props.data.username} - {props.data.name.firstname}
      </span>
    </Layout>
  );
}

export async function getStaticPaths() {
  let res = await fetch(`https://fakestoreapi.com/users`, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  let data = await res.json();
  const paths = data.map((artist) => ({
    params: { artistId: String(artist.id) },
  }));

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps(ctx) {
  let res = await fetch(
    `https://fakestoreapi.com/users/${ctx.params.artistId}`
  );
  let data = await res.json();

  return {
    props: {
      data,
    },
    revalidate: 5,
  };
}

export default ArtistSinglePage;
