import React from 'react';
import NextLink from 'next/link';

import Layout from 'src/components/Layout';
import { TrackConsumer, useTrack } from 'src/contexts';
import AnimateWrapper, {
  AnimateConsumer,
} from 'src/components/animate/AnimateWrapper';
import ListItemTrack from 'src/components/tracks/ListItemTrack';

function ArtistsPage() {
  const tracks = useTrack();
  return (
    <Layout title='Artists'>
      <div className='flex w-full flex-col justify-center md:flex-row md:justify-between'>
        <section className='w-full md:w-2/4 md:pr-3'>
          <AnimateWrapper timing='ease-out' typeAnimate='hidden-l'>
            <h1 className='rounded-md bg-emerald-300 p-2 text-center text-2xl font-medium tracking-wider text-zinc-600 dark:bg-slate-700 dark:text-zinc-400 md:p-4 md:text-5xl'>
              Artists
            </h1>
            <section className='flex flex-wrap '>
              <AnimateWrapper time={0.3} typeAnimate='hidden-b'>
                {tracks.fetchTracks.map((artist) => (
                  <div
                    key={artist.username}
                    className='my-5 flex w-full items-stretch justify-between border-b border-indigo-300 pb-1 text-lg font-medium md:w-[20rem]'
                  >
                    <NextLink href={`/artists/${artist.id}`}>
                      <a className='text-base text-indigo-800 transition duration-150 ease-out dark:text-indigo-100 dark:hover:text-indigo-400 md:text-lg'>
                        <span>{artist.username}</span>
                      </a>
                    </NextLink>
                    <span className='flex-grow' />
                    <TrackConsumer key={artist.username}>
                      {({ addFavTrack, addPlayingSong }) => (
                        <React.Fragment>
                          <button
                            title='Play Songs'
                            role='button'
                            className='btn mx-2 bg-blue-400 bg-opacity-90 shadow-inner transition duration-150 ease-out hover:bg-blue-600 active:bg-blank-light dark:bg-blue-700 dark:hover:bg-blue-500 dark:active:bg-blue-300 dark:active:text-black'
                            onClick={() => addPlayingSong(artist)}
                          >
                            <i className='bx bx-play bx-fade-right-hover p-0 text-sm md:text-xl' />
                          </button>
                          <button
                            title='Favorites'
                            disabled={artist.added}
                            className='btn btn--secondary mx-2 flex items-center justify-between disabled:opacity-25'
                            onClick={() => addFavTrack(artist)}
                          >
                            <i className='bx bx-star bx-burst-hover p-0 text-sm md:text-xl' />
                          </button>
                        </React.Fragment>
                      )}
                    </TrackConsumer>
                  </div>
                ))}
              </AnimateWrapper>
            </section>
          </AnimateWrapper>
        </section>
        <section className='w-full md:w-2/4 md:pl-3'>
          <TrackConsumer>
            {({ tracksPlayQueue, trackFavList }) =>
              (!!tracksPlayQueue.length || !!trackFavList.length) && (
                <AnimateWrapper typeAnimate='hidden-b'>
                  <h3 className='rounded-md bg-amber-200 p-2 text-center text-xl font-medium tracking-wider text-zinc-600 dark:bg-slate-700 dark:text-zinc-400 md:p-4 md:text-5xl'>
                    Songs
                  </h3>
                </AnimateWrapper>
              )
            }
          </TrackConsumer>
          <div className='flex justify-between gap-2 md:justify-evenly'>
            <TrackConsumer>
              {({ trackFavList, resetFavList, removeTrackOfList }) =>
                !!trackFavList.length && (
                  <ListItemTrack
                    listTrack={trackFavList}
                    onReset={resetFavList}
                    onRemoveTrack={removeTrackOfList}
                  />
                )
              }
            </TrackConsumer>

            <AnimateWrapper
              className='my-2'
              timing='ease-out'
              typeAnimate='hidden-r'
            >
              <TrackConsumer>
                {({ tracksPlayQueue, resetPlayList }) =>
                  !!tracksPlayQueue.length && (
                    <AnimateWrapper timing='ease-out' typeAnimate='hidden-r'>
                      <h3 className='text-center text-lg font-medium tracking-wide text-emerald-800 dark:text-emerald-400 md:text-2xl'>
                        Playing Songs
                      </h3>
                      <ul className='my-4 w-full list-decimal rounded-md py-5 pl-10 pr-5 shadow-md'>
                        {tracksPlayQueue.map((artist, index) => (
                          <li
                            className='hover:text-amber-500'
                            key={'artist-store-' + artist.id + index}
                          >
                            <AnimateWrapper
                              key={'artist-store-' + artist.id + index}
                              typeAnimate='hidden-r'
                            >
                              <NextLink
                                key={'artist-playing-' + (artist.id + index)}
                                href={`/artists/${artist.id}`}
                              >
                                <a className='text-sm font-medium text-slate-600 hover:text-amber-500 dark:text-slate-200 md:text-lg'>
                                  {artist.username}
                                </a>
                              </NextLink>
                            </AnimateWrapper>
                          </li>
                        ))}
                      </ul>
                      <AnimateConsumer>
                        {({ handleHidden }) => (
                          <button
                            onClick={() => handleHidden(resetPlayList)}
                            role='button'
                            title='Clear List tracks-artist'
                            className='btn flex bg-rose-500 text-blank-light transition-colors duration-150 ease-out hover:bg-rose-600 active:bg-red-600'
                          >
                            <i className='bx bx-play bx-fade-right-hover self-center text-sm md:mr-2 md:text-xl' />
                            <span className='text-sm md:text-lg'>
                              Reset Playing List
                            </span>
                          </button>
                        )}
                      </AnimateConsumer>
                    </AnimateWrapper>
                  )
                }
              </TrackConsumer>
            </AnimateWrapper>
          </div>
        </section>
      </div>
    </Layout>
  );
}

export default ArtistsPage;
