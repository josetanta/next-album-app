import React from 'react';
import NextLink from 'next/link';

import AnimateWrapper, {
  AnimateConsumer,
} from 'src/components/animate/AnimateWrapper';
import AlbumCard from 'src/components/AlbumCard';
import Layout from 'src/components/Layout';
import { TrackConsumer, useTrack } from 'src/contexts';

function AlbumsPage(props) {
  const tracks = useTrack();

  return (
    <Layout title='Albums' className='overflow-x-hidden'>
      <h1 className='text-lg font-bold tracking-widest md:text-5xl'>
        {props.title}
      </h1>
      {!!tracks.message && (
        <AnimateWrapper
          typeAnimate='hidden-r'
          time={0.4}
          timing='ease-in'
          className='fixed top-8 right-5 z-10 w-4/5 rounded-md bg-emerald-200 md:w-2/5'
          innerClassName='flex items-center gap-x-4 justify-around py-3 px-3 text-center text-base font-medium tracking-wide'
        >
          <i className='bx bx-check m-0 self-center rounded-full border border-emerald-700 p-0 text-3xl font-bold text-emerald-600' />
          <p
            className='text-base leading-6 dark:text-black md:text-lg'
            dangerouslySetInnerHTML={{ __html: tracks.message }}
          />
          <span className='flex-grow' />
          <NextLink href={`/artists`}>
            <a className='text-xs font-semibold text-green-800 underline'>
              Ver todos los artistas
            </a>
          </NextLink>
          <AnimateConsumer>
            {({ handleHidden }) => (
              <button
                onClick={() => handleHidden(tracks.resetMessage)}
                className='btn flex self-start bg-emerald-400 p-0 transition-colors duration-150 ease-out hover:bg-rose-500 dark:text-black'
              >
                <i className='bx bx-x m-0 self-center p-0 text-3xl' />
              </button>
            )}
          </AnimateConsumer>
        </AnimateWrapper>
      )}

      <div className='grid grid-cols-1 justify-items-center gap-2 md:grid-cols-2 md:overscroll-none xl:grid-cols-3'>
        <TrackConsumer>
          {({ fetchTracks }) => (
            <React.Fragment>
              {fetchTracks.map((artist, index) => (
                <AnimateWrapper
                  typeAnimate={index % 2 === 0 ? 'hidden-l' : 'hidden-r'}
                  key={artist.username}
                  innerClassName='m-0 flex justify-center p-0'
                >
                  <AlbumCard key={artist.username} data={artist} />
                </AnimateWrapper>
              ))}
            </React.Fragment>
          )}
        </TrackConsumer>
      </div>
    </Layout>
  );
}

AlbumsPage.defaultProps = {
  title: 'Albums',
};

export default AlbumsPage;
