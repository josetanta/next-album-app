import Layout from 'src/components/Layout';

function AlbumSinglePage() {
  return (
    <Layout>
      <h1 className='text-5xl font-medium tracking-wider'>AlbumSingle Page</h1>
    </Layout>
  );
}

export default AlbumSinglePage;
