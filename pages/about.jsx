import Layout from 'src/components/Layout';

function AboutPage() {
  return (
    <Layout title='About'>
      <h1 className='text-3xl font-bold text-blue-800'>Sobre Mí</h1>
    </Layout>
  );
}

export default AboutPage;
