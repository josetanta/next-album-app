module.exports = {
  content: ['./pages/**/*.{js,jsx}', './src/components/**/*.{js,jsx}'],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        'green-light': '#80ed99',
        'blank-light': '#f1f1f1',
      },
      screens: {
        xs: '460px',
      },
    },
    container: {
      padding: '2rem',
      center: true,
    },
  },
  plugins: [],
};
