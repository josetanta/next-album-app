const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      'd3eum8lucccgeh.cloudfront.net',
      'unsplash.com',
      'images.unsplash.com',
    ],
  },
  async redirects() {
    return [
      {
        source: '/home/:path*',
        destination: '/',
        permanent: true,
      },
      {
        source: '/index/:path*',
        destination: '/',
        permanent: true,
      },
      {
        source: '/album/:path*',
        destination: '/albums',
        permanent: true,
      },
      {
        source: '/artist/:path*',
        destination: '/artists',
        permanent: false,
      },
    ];
  },
};

module.exports = nextConfig;
