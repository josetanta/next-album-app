function withDynamic(Component) {
  return function HocComponent(props) {
    return <Component {...props} />;
  };
}

export default withDynamic;
