// TODO: Apply dark theme on app
export function changeThemeDark() {
  let inDark = document.documentElement.classList.contains('dark');
  if (inDark) {
    localStorage.setItem('theme', '');
    document.documentElement.classList.remove('dark');
  } else {
    localStorage.setItem('theme', 'dark');
    document.documentElement.classList.add('dark');
  }
}
