import React from 'react';

export const useMorphicEffect =
  typeof window !== 'undefined' ? React.useLayoutEffect : React.useEffect;
