import React from 'react';

export function useEvent(eventName, fn) {
  React.useEffect(() => {
    addEventListener(eventName, fn);

    return () => {
      removeEventListener(eventName, fn);
    };
  }, [eventName, fn]);
}
