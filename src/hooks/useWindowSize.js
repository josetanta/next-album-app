import React, { useCallback } from 'react';

export function useWindowSize() {
  const [size, setSize] = React.useState(() => {
    if (typeof window !== 'undefined')
      return { height: window.innerHeight, width: window.innerWidth };
    else return { height: 0, width: 0 };
  });

  const handleResize = useCallback(() => {
    setSize({ height: window.innerHeight, width: window.innerWidth });
  }, []);

  React.useEffect(() => {
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [handleResize]);

  return size;
}
