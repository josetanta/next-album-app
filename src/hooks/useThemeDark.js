import React from 'react';
import { changeThemeDark } from 'src/utils';

export function useThemeDark() {
  const [isDark, setIsDark] = React.useState(false);
  const classesRef = React.useRef(
    typeof window !== 'undefined' && document.documentElement.classList
  );

  const handleThemeClick = React.useCallback(() => {
    changeThemeDark();
    setIsDark(!isDark);
  }, [isDark]);

  React.useEffect(() => {
    let classes = classesRef.current;
    if (
      localStorage.theme === 'dark' ||
      (!('theme' in localStorage) &&
        window.matchMedia('(prefers-color-scheme: dark)').matches)
    ) {
      classes.add('dark');
      setIsDark(true);
    } else {
      classes.remove('dark');
      setIsDark(false);
    }

    return () => {
      setIsDark(false);
    };
  }, []);

  return { isDark, handleThemeClick };
}
