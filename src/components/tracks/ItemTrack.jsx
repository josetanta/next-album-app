import NextLink from 'next/link';
import AnimateWrapper from 'src/components/animate/AnimateWrapper';

function ItemTrack({ data: artist, onRemoveTrack }) {
  return (
    <AnimateWrapper
      className='my-3 rounded-md bg-zinc-200 dark:bg-slate-700'
      typeAnimate='hidden-l'
      innerClassName='flex flex-wrap justify-between items-center px-4 py-2'
      time={0.2}
    >
      <NextLink href={`/artists/${artist.id}`}>
        <a className='text-sm font-medium text-slate-600 hover:text-amber-500 dark:text-slate-200 md:text-lg'>
          {artist.username}
        </a>
      </NextLink>

      <button
        onClick={() => onRemoveTrack(artist.id)}
        className='flex flex-wrap p-1 text-rose-300 transition-colors duration-150 ease-in hover:text-rose-600 dark:rounded-full dark:text-rose-400 dark:hover:bg-rose-500 dark:hover:text-rose-100'
      >
        <i className='bx bx-trash self-center text-base md:text-xl' />
      </button>
    </AnimateWrapper>
  );
}

export default ItemTrack;
