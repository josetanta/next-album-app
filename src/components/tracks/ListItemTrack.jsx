import React from 'react';
import AnimateWrapper, {
  AnimateConsumer,
} from 'src/components/animate/AnimateWrapper';
import ItemTrack from './ItemTrack';

function ListItemTrack({ listTrack = [], onReset, onRemoveTrack }) {
  return (
    <AnimateWrapper typeAnimate='hidden-l'>
      <h3 className='text-center text-lg font-medium tracking-wide text-amber-700 dark:text-amber-300 md:text-2xl'>
        Artists Favorites
      </h3>
      <section className='my-4 w-full rounded-md p-2 px-1'>
        {listTrack.map((artist, index) => (
          <ItemTrack
            key={'artist-item-track-' + artist.id + index}
            data={artist}
            onRemoveTrack={onRemoveTrack}
          />
        ))}
        <AnimateConsumer>
          {({ handleHidden }) => (
            <button
              onClick={() => handleHidden(onReset)}
              role='button'
              title='Clear List tracks-artist'
              className='btn mt-3 flex bg-rose-500 text-blank-light transition-colors duration-150 ease-out hover:bg-rose-600 active:bg-red-600'
            >
              <i className='bx bx-trash-alt self-center text-sm md:mr-2 md:text-xl' />
              <span className='text-sm md:text-lg'>Reset List Favorites</span>
            </button>
          )}
        </AnimateConsumer>
      </section>
    </AnimateWrapper>
  );
}

export default React.memo(ListItemTrack);
