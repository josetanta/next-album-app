import NextLink from 'next/link';
import NextImage from 'next/image';
import { useTrack } from 'src/contexts';

function AlbumCard(props) {
  const track = useTrack();

  const handleAddClick = () => {
    track.addFavTrack(props.data);
  };

  const handleAddPlayingClick = () => {
    track.addPlayingSong(props.data);
  };

  return (
    <section
      className={`my-3 flex ${
        props.width || 'w-4/5'
      } flex-col rounded-md border px-5 py-3 text-gray-700 shadow-md dark:border-slate-800 dark:shadow-lg dark:shadow-slate-900`}
    >
      <header className='my-2 border-b border-b-gray-700 text-center text-2xl font-medium tracking-wide dark:border-0'>
        <NextLink href={`/artists/${props.artistId}`}>
          <a className='text-[25px] font-bold tracking-wider text-gray-700 transition duration-150 hover:animate-bounce hover:text-indigo-600 dark:text-gray-300 dark:hover:text-gray-500'>
            {props.data.username}
          </a>
        </NextLink>
      </header>
      <NextImage
        width='100%'
        height='100%'
        layout='responsive'
        loading='lazy'
        className='my-2 self-stretch rounded-md'
        alt={props.data.id}
        src='https://images.unsplash.com/photo-1528728935509-22fb14722a30?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=718&q=80'
      />
      <div className='py-2 dark:text-gray-300'>
        <span className='font-medium tracking-wide text-zinc-700 dark:text-zinc-200'>
          Description
        </span>
        <p className='text-justify'>
          Content Lorem, ipsum dolor sit amet consectetur adipisicing elit.
          Reprehenderit ea molestias illo commodi culpa facere rerum velit, cum
          repellat hic, optio, et nesciunt. Lorem ipsum, dolor sit amet
          consectetur adipisicing elit. Consequatur iste a repudiandae quam hic,
          libero sed corrupti fugiat sint? Quam, maxime.
        </p>
      </div>
      <div className='my-3 flex justify-between'>
        <NextLink href={`/artists/${props.data.id}`} scroll>
          <a className='btn btn--primary flex items-center justify-between'>
            <i className='bx bxs-album bx-spin-hover mr-2 text-xl' />
            <span>View Artist</span>
          </a>
        </NextLink>
        <span className='flex-grow' />
        <button
          title='Play Songs'
          role='button'
          className='btn mx-2 flex items-center justify-between bg-blue-400 bg-opacity-90 transition duration-150 ease-out hover:bg-blue-600 active:bg-blank-light dark:bg-blue-700 dark:hover:bg-blue-500 dark:active:bg-blue-300 dark:active:text-black'
          onClick={handleAddPlayingClick}
        >
          <i className='bx bx-play bx-fade-right-hover p-0 text-sm text-white md:text-xl' />
        </button>
        <button
          disabled={props.data.added}
          title='Favorites'
          className='btn btn--secondary flex items-center justify-between disabled:opacity-30'
          onClick={handleAddClick}
        >
          <i className='bx bx-star bx-burst-hover' />
        </button>
      </div>
    </section>
  );
}

export default AlbumCard;
