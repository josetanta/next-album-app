import NextLink from 'next/link';

function Footer() {
  return (
    <footer className='mt-10 border-t border-t-green-light pt-5'>
      <section className='flex w-full items-start justify-between'>
        <div className='flex w-1/2 flex-col items-start'>
          <ul className='block px-5 text-sm font-medium tracking-wide text-zinc-700 dark:text-zinc-300'>
            <li className='my-2'>
              <NextLink href='/'>
                <a className='transition duration-150 ease-out hover:text-amber-800'>
                  <i className='bx bx-home mr-2' />
                  <span className='underline hover:no-underline'>Home</span>
                </a>
              </NextLink>
            </li>
            <li className='my-2'>
              <NextLink href='/albums'>
                <a className='transition duration-150 ease-out hover:text-indigo-800'>
                  <i className='bx bx-user mr-2' />
                  <span className='underline hover:no-underline'>Albums</span>
                </a>
              </NextLink>
            </li>
            <li className='my-2'>
              <NextLink href='/artists'>
                <a className='transition duration-150 ease-out hover:text-rose-600'>
                  <i className='bx bx-album mr-2' />
                  <span className='underline hover:no-underline'>Artists</span>
                </a>
              </NextLink>
            </li>
          </ul>
        </div>
        <div className='flex w-1/2 flex-col'>
          <p className='text-xs'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore,
            vero architecto natus corporis iusto fuga, placeat repellat earum
            explicabo consequatur qui aut magni corrupti aliquid quis nostrum
            obcaecati! Saepe consectetur fugiat sit?
          </p>
        </div>
      </section>
      <div className='my-5 text-center text-sm tracking-wide text-zinc-700 dark:text-zinc-400'>
        &copy;
        <a
          href='mailto: jose.tanta.27@unsch.edu.pe'
          className='font-medium transition-colors duration-100 hover:text-sky-700'
        >
          josetanta
        </a>
        - 2022
      </div>
    </footer>
  );
}

export default Footer;
