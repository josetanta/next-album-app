import React from 'react';
import NextLink from 'next/link';
import { useThemeDark, useWindowSize } from 'src/hooks';
import AnimateWrapper from './animate/AnimateWrapper';
import Button from './Button';

let links = [
  {
    href: '/',
    name: 'Home',
    icon: <i className='bx bx-home bx-tada-hover text-lg no-underline' />,
  },
  {
    href: '/artists',
    name: 'Artists',
    icon: <i className='bx bx-user bx-tada-hover text-lg no-underline' />,
  },
  {
    href: '/albums',
    name: 'Albums',
    icon: <i className='bx bx-album bx-spin-hover text-lg no-underline' />,
  },
];

/**
 * Navbar Component
 * @returns JSX.Element
 */
function Navbar() {
  const [show, setShow] = React.useState(false);
  const [showList, setShowList] = React.useState(false);
  const size = useWindowSize();
  const theme = useThemeDark();

  const handleShowListClick = React.useCallback(() => {
    setShowList(!showList);
  }, [showList]);

  React.useEffect(() => {
    if (size.width <= 780) {
      setShow(true);
    } else {
      setShow(false);
    }
  }, [show, size.width]);

  return (
    <React.Fragment>
      <header>
        <nav className='min-w-screen container mb-2 flex h-[80px] items-center justify-between border-b border-b-green-light'>
          <NextLink href='/'>
            <h2 className='cursor-pointer text-lg font-semibold tracking-wider text-slate-700 transition duration-100 hover:text-gray-500 dark:text-slate-300 dark:hover:text-gray-500 md:text-3xl'>
              The PlayList
            </h2>
          </NextLink>
          {!showList && show ? null : (
            <AnimateWrapper
              className='absolute top-16 right-20 z-10 w-[10rem] rounded-md border border-slate-500 bg-slate-200 opacity-75 transition-all duration-100 ease-out dark:bg-slate-600 md:relative md:top-0 md:right-0 md:w-auto md:rounded-none md:border-0 md:bg-inherit dark:md:bg-inherit'
              innerClassName='flex flex-col items-center gap-x-2 md:flex md:w-auto md:flex-row md:justify-between md:gap-x-6 md:opacity-100'
              timing='linear'
            >
              {links.map((link, index) => (
                <NextLink
                  href={link.href}
                  passHref
                  key={link.name + (index + 1)}
                >
                  <a className='nav__link'>
                    {link.icon}
                    <span>{link.name}</span>
                  </a>
                </NextLink>
              ))}
              <Button
                onClick={theme.handleThemeClick}
                className='my-4 flex items-center'
              >
                <i
                  className={`bx ${
                    theme.isDark
                      ? 'bx-sun bx-spin-hover'
                      : 'bx-moon bx-flashing-hover'
                  } text-lg`}
                />
              </Button>
            </AnimateWrapper>
          )}
          <button
            onClick={handleShowListClick}
            title='Menu'
            aria-labelledby='menu-bar'
            className='self-center rounded-md border border-slate-500 py-1 px-2 leading-5 transition duration-150 ease-out hover:bg-slate-300 md:hidden'
          >
            <i className='bx bx-menu text-lg' />
          </button>
        </nav>
      </header>
    </React.Fragment>
  );
}

export default Navbar;
