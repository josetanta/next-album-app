import React from 'react';
import NextHead from 'next/head';
import Navbar from 'src/components/Navbar';

function Layout({ title = 'Home', children = React.Children, className = '' }) {
  return (
    <React.Fragment>
      <NextHead>
        <title>The PlayList | {title}</title>
        <meta name={'page-' + title} content={title} />
      </NextHead>
      <Navbar />
      <main className={`max-w-screen container ${className}`}>{children}</main>
    </React.Fragment>
  );
}

export default Layout;
