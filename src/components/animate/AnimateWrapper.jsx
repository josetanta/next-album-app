import React from 'react';
import { useMorphicEffect } from 'src/hooks';

import styles from 'styles/animation.module.css';

const animateWrapper = React.createContext({
  handleHidden(_handler) {},
});
animateWrapper.displayName = 'Animate';
export const AnimateConsumer = animateWrapper.Consumer;

function getTypeAnimation(type = 'hidden') {
  let styleAnimate = {
    'hidden-r': styles.animate__hidden__right,
    'hidden-l': styles.animate__hidden__left,
    'hidden-b': styles.animate__hidden__bottom,
    'hidden-t': styles.animate__hidden__top,
  };

  return String(styleAnimate[type] || styles.animate__hidden);
}

function getFrameAnimation(type = 'hidden') {
  let styleAnimate = {
    'hidden-r': styles.animate__active__right,
    'hidden-l': styles.animate__active__left,
    'hidden-b': styles.animate__active__bottom,
    'hidden-t': styles.animate__active__top,
    hidden: styles.animate__active,
  };

  return String(styleAnimate[type]);
}

function AnimateWrapper({
  time = 0.5,
  typeAnimate = 'hidden',
  timing = 'ease',
  innerClassName = '',
  className = '',
  styles = {},
  innerStyles = {},
  children,
}) {
  const wrapperParentRef = React.useRef();
  const wrapperRef = React.useRef();
  const isMountedRef = React.useRef(false);
  const stylesAnimateRef = React.useRef({
    typeAnimation: getTypeAnimation(typeAnimate),
    frameAnimation: getFrameAnimation(typeAnimate),
  });

  const handleHidden = React.useCallback(
    (_handler) => {
      let wrapper = wrapperRef.current;
      let stylesAnimate = stylesAnimateRef.current;

      wrapper.classList.add(stylesAnimate.typeAnimation);

      let timeout = setTimeout(() => {
        wrapper.classList.remove(stylesAnimate.typeAnimation);
        _handler?.();
        clearTimeout(timeout);
      }, time * 1000);
    },
    [time]
  );

  useMorphicEffect(() => {
    isMountedRef.current = true;
    return () => {
      isMountedRef.current = false;
    };
  }, []);

  useMorphicEffect(() => {
    let wrapperParent = wrapperParentRef.current;
    let stylesAnimate = stylesAnimateRef.current;
    isMountedRef.current &&
      wrapperParent.classList.add(
        stylesAnimate?.frameAnimation,
        stylesAnimate?.typeAnimation
      );
    return () => {
      isMountedRef.current &&
        wrapperParent.classList.remove(
          stylesAnimate?.frameAnimation,
          stylesAnimate?.typeAnimation
        );
      wrapperRef.current = null;
      wrapperParentRef.current = null;
      stylesAnimateRef.current = undefined;
    };
  }, []);

  return (
    <animateWrapper.Provider value={{ handleHidden }}>
      <div
        ref={wrapperParentRef}
        style={{
          animationTimingFunction: timing,
          animationDuration: `${time}s`,
          animationDelay: `${time / 4}s`,
          ...styles,
        }}
        className={className}
      >
        <div
          className={innerClassName}
          ref={wrapperRef}
          style={{
            transitionDuration: `${time}s`,
            transitionTimingFunction: timing,
            transitionDelay: `${time / 4}s`,
            ...innerStyles,
          }}
        >
          {children}
        </div>
      </div>
    </animateWrapper.Provider>
  );
}

export default AnimateWrapper;
