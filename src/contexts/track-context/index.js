import React from 'react';
import { trackState } from './reducers';

export * from './TrackProvider';

export const trackContext = React.createContext({
  ...trackState,
  addFavTrack(_item) {},
  resetFavList() {},
  resetPlayList() {},
  addPlayingSong(_item) {},
  resetMessage() {},
  removeTrackOfList(_trackId) {},
});

trackContext.displayName = 'Track';

export const TrackConsumer = trackContext.Consumer;

export function useTrack() {
  return React.useContext(trackContext);
}
