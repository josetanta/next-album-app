export const trackState = {
  trackFavList: [],
  tracksPlayQueue: [],
  fetchTracks: [],
  message: '',
};

export function trackReducer(state = trackState, action) {
  switch (action.type) {
    case 'add-track-fav': {
      let existTrack = state.trackFavList.find(
        (x) => action.payload.track.id === x.id
      );

      let addTrack = Object.assign({}, action.payload.track, { added: true });

      let trackFavList = existTrack
        ? state.trackFavList
        : state.trackFavList.concat(addTrack);

      let fetchTracks = state.fetchTracks.map((item) =>
        item.id === addTrack.id
          ? Object.assign({}, item, { added: true })
          : item
      );

      return {
        ...state,
        trackFavList,
        fetchTracks,
        message: action.payload.message,
      };
    }

    case 'fetcher-tracks': {
      let fetchTracks = action.payload.tracksList;
      fetchTracks = fetchTracks.map((item) =>
        Object.assign({}, item, { added: false })
      );
      return { ...state, fetchTracks };
    }

    case 'add-playing-track': {
      // TODO: PLaying Songs
      let tracksPlayQueue = state.tracksPlayQueue.concat(action.payload.track);
      return { ...state, tracksPlayQueue, message: action.payload.message };
    }

    case 'reset-playing-list': {
      // TODO: Remove songs of list queue playing song
      return { ...state, tracksPlayQueue: [], message: '' };
    }

    case 'reset-track-list': {
      let fetchTracks = state.fetchTracks.map((item) =>
        Object.assign({}, item, { added: false })
      );
      return { ...state, trackFavList: [], fetchTracks, message: '' };
    }

    case 'remove-track-list': {
      let trackFavList = state.trackFavList.filter(
        (item) => Number(item.id) !== Number(action.payload.trackId)
      );

      let fetchTracks = state.fetchTracks.map((item) =>
        item.id === action.payload.trackId
          ? Object.assign({}, item, { added: false })
          : item
      );

      return { ...state, trackFavList, fetchTracks };
    }

    case 'reset-message':
      return { ...state, message: '' };

    default:
      return state;
  }
}
