import React from 'react';
import { trackContext } from './index';
import { trackReducer, trackState } from './reducers';

export function initState(state) {
  return { ...state };
}

export function TrackProvider(props) {
  const [state, dispatch] = React.useReducer(
    trackReducer,
    trackState,
    initState
  );

  const addFavTrack = (track) => {
    let message = `Se añadio a su artista favorito <b>${track.username}</b>`;
    dispatch({ type: 'add-track-fav', payload: { track, message } });
  };

  const addPlayingSong = (track) => {
    let message = `Se añadio a la cola para la reproducción de <b>${track.username}</b>`;
    dispatch({ type: 'add-playing-track', payload: { track, message } });
  };

  const resetFavList = () => {
    dispatch({ type: 'reset-track-list' });
  };

  const resetPlayList = () => {
    dispatch({ type: 'reset-playing-list' });
  };

  const resetMessage = () => {
    dispatch({ type: 'reset-message' });
  };

  const removeTrackOfList = React.useCallback((trackId) => {
    dispatch({ type: 'remove-track-list', payload: { trackId } });
  }, []);

  React.useEffect(() => {
    fetch('https://fakestoreapi.com/users')
      .then((res) => res.json())
      .then((data) =>
        dispatch({
          type: 'fetcher-tracks',
          payload: { tracksList: data },
        })
      )
      .catch((err) =>
        dispatch({
          type: 'fetcher-tracks',
          payload: { tracksList: [err] },
        })
      );
  }, []);

  let value = {
    ...state,
    resetFavList,
    addFavTrack,
    addPlayingSong,
    resetPlayList,
    resetMessage,
    removeTrackOfList,
  };

  return (
    <trackContext.Provider value={value}>
      {props.children}
    </trackContext.Provider>
  );
}
